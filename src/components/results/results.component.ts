import { Component, Input } from '@angular/core';
import { IOmdbMovie } from '../../models/omdbmovie.interface';

interface IResultsComponent{
  orderDesc: boolean;
  isSorted(orderBy: string): boolean;
  sort(orderBy: string): void;
}

@Component({
  selector: 'results',
  templateUrl: 'src/components/results/results.component.html'
})
export class Results implements IResultsComponent {  
  orderDesc: boolean;
  @Input() orderBy: string;
  @Input() results: IOmdbMovie[];

  constructor(){
    this.orderDesc = false;
  }

  // you need to implement your own order by filter, check this out
  // http://stackoverflow.com/questions/32882013/angular-2-sort-and-filter 
  // https://github.com/nicolas2bert/angular2-orderby-pipe/blob/master/app/orderby.ts

  isSorted(orderBy: string): boolean{
    return this.orderBy === orderBy;
  }

  sort(orderBy: string): void{
    if (orderBy === this.orderBy) {
      this.orderDesc = !this.orderDesc;
    } else {
      this.orderBy = orderBy;
      this.orderDesc = false;
    }
  }
}