import { Component, Output, EventEmitter } from '@angular/core';

const KEY_ENTER = 13; 

interface ISearchComponent {
  searchText: string;
  search(): void;
  searchKeyup(event: KeyboardEvent): void;
}

@Component({
  selector: 'search',
  templateUrl: 'src/components/search/search.component.html'
})
export class Search implements ISearchComponent {
  searchText: string;
  @Output() searchEvent = new EventEmitter();

  search(): void{
    this.searchEvent.emit({
      value: this.searchText
    });
  }

  searchKeyup(event: KeyboardEvent): void{
    if (event.keyCode == KEY_ENTER) { // enter
      this.search();
    }    
  }
}