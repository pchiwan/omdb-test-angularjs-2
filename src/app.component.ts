import { Component } from '@angular/core';
// Add the RxJS Observable operators we need in this app.
import './rxjs-operators';

import { Header, MovieDetail, MovieReviews, Results, Search } from './components';
import { OmdbService, ReviewService } from './services';
import { IOmdbMovie, IOmdbSearchResult, IReview } from './models';

interface IAppComponent{
  loading: boolean;
  loadingBottom: boolean;
  results: IOmdbMovie[];
  reviews: IReview[];
  selectedMovie: IOmdbMovie;
  fetchMovies(): void;
  fetchMovieById(id: string): void;
}

@Component({
  selector: 'my-app',
  templateUrl: 'src/app.component.html',
  directives: [Header, Search, Results, MovieDetail, MovieReviews],
  providers: [OmdbService, ReviewService]
})
export class AppComponent {
  loading: boolean;
  loadingBottom: boolean;
  results: IOmdbMovie[];
  reviews: IReview[];
  selectedMovie: IOmdbMovie;
  
  constructor(private omdbService: OmdbService, private reviewService: ReviewService) {
    this.loading = false;
    this.loadingBottom = false;
    this.results = [];
    this.reviews = [];
  }

  fetchMovies(event: any): void {
    if (event.value) {
      this.loading = true;
      this.omdbService.search(event.value)
        .subscribe((data: IOmdbSearchResult) => {          
          this.results = data.Search;
          this.loading = false;
        }, (error: any) => {
          console.log(error);
        });
    }
  }

  fetchMovieById(id: string): void {
    this.loadingBottom = true;
    this.reviews = [];
    
    const p1 = this.omdbService.getById(id)
      .toPromise()      
      .then((data: IOmdbMovie) => {
        this.selectedMovie = data;
        this.selectedMovie.Poster = this.omdbService.getPosterUrl(id);
        this.selectedMovie.ImdbUrl = this.omdbService.getImdbUrl(id);        
      });

    const p2 = this.reviewService.getByImdbId(id)
      .toPromise()
      .then(function (data) {
        this.reviews = data;					
      });

    Promise.all([p1, p2]).then(() => {
      this.loadingBottom = false;
    });
  }
}			
