export interface IOmdbMovie{
  imdbID: string;
  Actors: string;
  Awards: string;
  Country: string;
  Director: string;
  ImdbUrl: string;
  Language: string;
  Metascore: string;
  Plot: string;
  Poster: string;
  Released: string;
  Runtime: string;
  Title: string;
  Writer: string;
  Year: number;  
}

export interface IOmdbSearchResult{
  Search: IOmdbMovie[];
  Response: string;
  totalResults: string;
}
