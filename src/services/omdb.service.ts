import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import '../rxjs-operators';

import config from './config';
import { BaseService, IBaseService } from './base.service';
import { IOmdbMovie, IOmdbSearchResult } from '../models';

interface IOmdbService extends IBaseService {
  search(searchText: string): Observable<IOmdbSearchResult>;
  getByTitle(title: string): Observable<IOmdbMovie>;
  getById(imdbID: string): Observable<IOmdbMovie>;
  getPosterUrl(imdbID: string): string;
  getImdbUrl(imdbID: string): string;
}

@Injectable()
export class OmdbService extends BaseService implements IOmdbService {
	constructor(http: Http) {
		super(http);
	}

	search(searchText: string): Observable<IOmdbSearchResult> {
		return this.get(`${config.omdbApiUrl}s=${searchText}`);			
	}
	
	getByTitle(title: string): Observable<IOmdbMovie> {
		return this.get(`${config.omdbApiUrl}t=${title}`);
	}
	
	getById(imdbID: string): Observable<IOmdbMovie> {
		return this.get(`${config.omdbApiUrl}i=${imdbID}`);
	}

	getPosterUrl(imdbID: string): string {
		return this.imgApiUrl(imdbID);
	}

	getImdbUrl(imdbID: string): string {
		return this.imdbUrl(imdbID);
	}
}