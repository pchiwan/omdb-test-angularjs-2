import config from './config';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

export interface IBaseService {
  imgApiUrl(imdbID: string): string;
  imdbUrl(imdbID: string): string;
  get(url: string): Observable<any>;
}

export class BaseService implements IBaseService {  
  constructor(public http: Http) {}

  public imgApiUrl(imdbID: string) {
    return `http://img.omdbapi.com/?i=${imdbID}&apikey=${config.omdbApiKey}`;
  }

  public imdbUrl(imdbID: string) {
    return `http://imdb.com/title/${imdbID}/`;
  }

  get(url: string): Observable<any> {
		return this.http.get(url)
			.map((res: Response) => {
				return res.json()
			})
			.catch((error: any) => {
				let errMsg = (error.message) 
					? error.message 
					: (error.status ? `${error.status} - ${error.statusText}` : 'Server error');
				console.error(errMsg); // log to console instead
				return Observable.throw(errMsg);
			});
	}
}